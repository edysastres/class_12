const http = require('http');
const port = 8000;

function responseHandler(request, response){
	console.log(request.method);
	console.log(request.url);
	response.write('Hello World');
	response.end();
}

const server = http.createServer(responseHandler);

/*
	http.createServer(function (req, res) {
		res.write('Hello World');
		res.end()
	})
*/
server.listen(port);
